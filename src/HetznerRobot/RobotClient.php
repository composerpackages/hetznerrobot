<?php

namespace ETailors\HetznerRobot;

class RobotClient extends BaseRestClient
{

    const VERSION="1.0.0";
    /**
     * Class constructor
     *
     * @param $url      Robot webservice url
     * @param $login    Robot login name
     * @param $password Robot password
     * @param $verbose
     */
    public function __construct($url, $login, $password, $verbose = false)
    {
        parent::__construct($url, $login, $password, $verbose);
        $this->setHttpHeader('Accept', 'application/json');
        $this->setHttpHeader('User-Agent', 'HetznerRobotClient/' . self::VERSION);
    }

    public function getServers()
    {
        $url = $this->baseUrl . "server";
        return $this->get($url);
    }
}
